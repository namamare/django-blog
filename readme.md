# Post git clone setup

## OS Setup

#### Update pip
`> python3 -m pip install --upgrade`

## Virtual Environment Setup

#### Create project folder
`> mkdir $project_dir_name`

Now step into this newmly created dir
`> cd $project_dir_name`

#### Create a virtual environment 
Lets create a virtual evnvironment 
`> python3 -m venv $.env` 
A new folder named -env was created and will hold python requirents for this project. This way we dont mess with OS's python installation.
Now active the environment.
`> . .env/bin/activate`
You can deactivate it at any time with
`> deactivate`

#### Install requiremnents
`> pip3 install -R requirements.txt`  
And now we have all the necessary dependencies installed

## Install and Configure Postgres
Pacman `> sudo pacman -S postgresql`  
APT `> sudo apt install postgresql`

Switch to OS user *posgres* ...
`> sudo su postgres -l`
...and create the database engine...
`> initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data/'`
...where $LANG is a local variable fot ex. en_US.UTF-8, and get back to your user with  `> quit`

#### Make Postgresql run as a serevice
`> sudo systemctl enable --now postgresql.service`
Check the version `> psql --version`
and that the service is runnint `> systemctl service status postgres`

#### Create database role and user
Enter psql console as user OS user *postres* 
`> sudo su postgres -c psql`

Create the USER `# CREATE USER <username> WITH PASSWORD '<a_passowrd>';`
Now give the user a role which will grant permissions `# ALTER ROLE <username> WITH CREATEDB;`	

###### Postgres Roles
You can check roles in PSQL console with `# \du` that should echo something similar to the bellow options
> * Superuser
> * Create role 
> * Create DB
> * Replication
> * Bypass RLS

### Spin Up Django
After the following command you should be able to visit it n the browser...
`> python3 manage.py runserver`
...and see similar output
> Django version 3.2.3, using settings 'django_project.settings'
> Starting development server at http://127.0.0.1:8000/
> Quit the server with CONTROL-C.

### Migrate 
Let's tell our database what is should create. First we create the django migrations
`> python3 manage.py createmigrations`
and then pushed them to the database width
`> python3 manaage.py migrate`


# GIT repo tune
## Setup SSH keys
### Craeate SSH Key Pair

### Add to the repository

### Set GIT Upstream Address
Local GIT client needs to know where to push the code via ssh. To do that we set the `> git --upstream` parameter wuth the repo address.
ex. `> git push --set-upstream git@gitlab.com:namamare/django-blog.git`
You can grab the address from your git web client.

### 
