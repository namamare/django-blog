import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_project.settings")
django.setup()

def setup():
    module = os.path.split(os.path.dirname(__file__))[-1]
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{}.settings".format(module))
    import django
    django.setup()

setup()
print("Done Setup")

from blog.models import Post, Category
from users.models import User
import json
from random import randrange
  
# Opening JSON file
f = open('mock_blog_data.json',)

# returns JSON object as 
# a dictionary
data = json.load(f)

# Iterating through the json
# list
for i in data['categories']:
    if (Category.objects.filter(title=i['title'])):
        continue
    n = Category(title=i['title'])
    n.save()

for i in data['posts']:
    if (Post.objects.filter(title=i['title'])):
        continue
    n = Post(title=i['title'],
            date_posted=i['date_posted'],
             author=User.objects.filter(username="namamare")[0],
             category=Category.objects.filter(title=data['categories'][randrange(0,len(data['categories']))]['title'])[0],
			 content=i['content'])
    n.save()


# Closing file
f.close()
