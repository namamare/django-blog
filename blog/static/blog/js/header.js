

$(document).ready(function () { 

    var categories_loaded = false;
    if (!categories_loaded) {
        getHeaderCategories();
        console.log('calling categories');
    }

    var header = $('header');
    var headerNav = $('.header-nav');
    var navItemsWrapper = $('.nav-categories-wrapper');
    var links = $('.nav-item');
    // responsive navigation 
    $("#nav-toogler").on('click', function(elem){ 
        $(this).toggleClass('open'); 
        if (header.height() < 100)
        {
            header.addClass('open');
            headerNav.addClass('open');
            links.addClass('open');
        } 
        else 
        {
            header.removeClass('open');  
            headerNav.removeClass('open');
            links.removeClass('open');
            navItemsWrapper.removeClass('open');
 
        }
    });

    // show/hide header titles for resizing maniacs
    $( window ).resize(function() {
        header = $('.site-header');
        links = $('.nav-item');

        if(header.width() <= 768 && header.height() == $(document).height())
        {
            links.addClass({'display' : 'block', });
        }
        else if(header.width() <= 769 && header.height() == 80)
        {
            links.css({'display' : 'none',});
        }
        else 
        {
            links.css({'display' : 'block',});
        }
    });
    
    $('.nav-item.nav-categories').on('mouseenter', function(e){ 
         
        var navItems = $(this);
        header.addClass('open');
        navItems.addClass('open');
        navItemsWrapper.addClass('open'); 
        // nav.find( )
    });

    $('.nav-categories-wrapper, header').on('mouseleave', function(e){ 
        if($(window).width() > 768) {  

            setTimeout(
                function(){
                    header.removeClass('open');  
                    headerNav.removeClass('open');
                    navItemsWrapper.removeClass('open');
                    links.removeClass('open');
                 },300);

        
        }
    });
}); 

function getHeaderCategories(){ 
        
    $.ajax({
        url: "/getHeaderCategories",
        type: "GET",  
        data: { 
            // 'csrfmiddlewaretoken':$("input[name=csrfmiddlewaretoken]").val(),    
     },
        success: function(xhr){ 
            var elem = $('.nav-categories-wrapper'); 
            console.log(xhr);
            // console.log(cats);
            for (const [key, value] of Object.entries(xhr)) {
                
                console.log(key);
                console.log(value);
                elem.append('<a class="nav-categories-item" href="/' + value.idx + '">' + value.name + '</a>'); 
            }
            // categories_loaded = true;

        }
    });



} 


