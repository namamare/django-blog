
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.timezone import now
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField


class Category(models.Model):

    class Meta:
        db_table = 'Category'

    verbose_name_plural = "Categories"
    
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, unique=True)
    order = models.PositiveSmallIntegerField(default=32767)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-category', kwargs={'pk':self.pk})


class Post(models.Model):

    class Meta:
        db_table = 'Post'

    id = models.AutoField(primary_key=True)
    verbose_name_plural = "Posts"

    title = models.CharField(max_length=100)
    content = RichTextUploadingField(blank=True, null=True) 
    author = models.ForeignKey(User, on_delete=models.CASCADE) 
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None) 
    views = models.PositiveSmallIntegerField(default=0)

    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
